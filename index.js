// Bài 1: Sắp xếp 3 số theo thứ tự tăng dần
document.getElementById("btnSapXepB1").onclick = function () {
  var soDauTienB1 = document.getElementById("soDauTienB1").value * 1;
  var soThuHaiB1 = document.getElementById("soThuHaiB1").value * 1;
  var soThuBaB1 = document.getElementById("soThuBaB1").value * 1;
  var biggestNumB1 = null;
  var biggerNumB1 = null;
  var smallerNumB1 = null;

  if (soDauTienB1 > soThuHaiB1 && soThuHaiB1 > soThuBaB1) {
    biggestNumB1 = soDauTienB1;
    biggerNumB1 = soThuHaiB1;
    smallerNumB1 = soThuBaB1;
    document.getElementById("showDaySoB1").innerHTML =
      smallerNumB1 + ", " + biggerNumB1 + ", " + biggestNumB1;
  } else if (soDauTienB1 > soThuBaB1 && soThuBaB1 > soThuHaiB1) {
    biggestNumB1 = soDauTienB1;
    biggerNumB1 = soThuBaB1;
    smallerNumB1 = soThuHaiB1;
    document.getElementById("showDaySoB1").innerHTML =
      smallerNumB1 + ", " + biggerNumB1 + ", " + biggestNumB1;
  } else if (soThuHaiB1 > soDauTienB1 && soDauTienB1 > soThuBaB1) {
    biggestNumB1 = soThuHaiB1;
    biggerNumB1 = soDauTienB1;
    smallerNumB1 = soThuBaB1;
    document.getElementById("showDaySoB1").innerHTML =
      smallerNumB1 + ", " + biggerNumB1 + ", " + biggestNumB1;
  } else if (soThuHaiB1 > soThuBaB1 && soThuBaB1 > soDauTienB1) {
    biggestNumB1 = soThuHaiB1;
    biggerNumB1 = soThuBaB1;
    smallerNumB1 = soDauTienB1;
    document.getElementById("showDaySoB1").innerHTML =
      smallerNumB1 + ", " + biggerNumB1 + ", " + biggestNumB1;
  } else if (soThuBaB1 > soDauTienB1 && soDauTienB1 > soThuHaiB1) {
    biggestNumB1 = soThuBaB1;
    biggerNumB1 = soDauTienB1;
    smallerNumB1 = soThuHaiB1;
    document.getElementById("showDaySoB1").innerHTML =
      smallerNumB1 + ", " + biggerNumB1 + ", " + biggestNumB1;
  } else {
    biggestNumB1 = soThuBaB1;
    biggerNumB1 = soThuHaiB1;
    smallerNumB1 = soDauTienB1;
    document.getElementById("showDaySoB1").innerHTML =
      smallerNumB1 + ", " + biggerNumB1 + ", " + biggestNumB1;
  }
};

// Bài 2: Chương trình chào hỏi
document.getElementById("btnChaoHoiB2").onclick = function () {
  var thanhVienB2 = document.getElementById("thanhVienB2").value;
  switch (thanhVienB2) {
    case "Bố":
      document.getElementById("showChaoHoiB2").innerHTML =
        "Chào " + thanhVienB2 + "!";
      break;
    case "Mẹ":
      document.getElementById("showChaoHoiB2").innerHTML =
        "Chào " + thanhVienB2 + "!";
      break;
    case "Anh trai":
      document.getElementById("showChaoHoiB2").innerHTML =
        "Chào " + thanhVienB2 + "!";
      break;
    case "Em gái":
      document.getElementById("showChaoHoiB2").innerHTML =
        "Chào " + thanhVienB2 + "!";
      break;
    default:
      document.getElementById("showChaoHoiB2").innerHTML = "Chào Người lạ ơi!";
  }
};

// Bài 3: Đếm số chẵn & lẻ
document.getElementById("btnChanLeB3").onclick = function () {
  var soThuNhatB3 = document.getElementById("soThuNhatB3").value * 1;
  var soThuHaiB3 = document.getElementById("soThuHaiB3").value * 1;
  var soThuBaB3 = document.getElementById("soThuBaB3").value * 1;
  var intSoChan = 0;
  if (soThuNhatB3 % 2 == 0) {
    intSoChan += 1;
  }
  if (soThuHaiB3 % 2 == 0) {
    intSoChan += 1;
  }
  if (soThuBaB3 % 2 == 0) {
    intSoChan += 1;
  }
  document.getElementById("showChanLeB3").innerHTML =
    "Có " + intSoChan + " số chẵn & " + (3 - intSoChan) + " số lẻ.";
};

// Bài 4: Dự đoán hình tam giác
document.getElementById("btnDoanHinhB4").onclick = function () {
  var canhThuNhatB4 = document.getElementById("canhThuNhatB4").value * 1;
  var canhThuHaiB4 = document.getElementById("canhThuHaiB4").value * 1;
  var canhThuBaB4 = document.getElementById("canhThuBaB4").value * 1;
  var hinhTamGiac = document.getElementById("showDoanHinhB4");
  // Triangle condition
  if (
    canhThuNhatB4 + canhThuHaiB4 <= canhThuBaB4 ||
    canhThuHaiB4 + canhThuBaB4 <= canhThuNhatB4 ||
    canhThuNhatB4 + canhThuBaB4 <= canhThuHaiB4
  ) {
    alert(
      "Độ dài cách cạnh không tạo thành hình tam giác. Vui lòng nhập lại giá trị các cạnh"
    );
  }
  // Guess Triangle type based on sides value
  if (canhThuNhatB4 == canhThuHaiB4 && canhThuNhatB4 == canhThuBaB4) {
    hinhTamGiac.innerHTML = "Hình tam giác đều";
  } else if (
    canhThuNhatB4 == canhThuHaiB4 ||
    canhThuNhatB4 == canhThuBaB4 ||
    canhThuHaiB4 == canhThuBaB4
  ) {
    hinhTamGiac.innerHTML = "Hình tam giác cân";
  } else if (
    Math.pow(canhThuNhatB4, 2) ==
      Math.pow(canhThuHaiB4, 2) + Math.pow(canhThuBaB4, 2) ||
    Math.pow(canhThuHaiB4, 2) ==
      Math.pow(canhThuNhatB4, 2) + Math.pow(canhThuBaB4, 2) ||
    Math.pow(canhThuBaB4, 2) ==
      Math.pow(canhThuHaiB4, 2) + Math.pow(canhThuNhatB4, 2)
  ) {
    hinhTamGiac.innerHTML = "Hình tam giác vuông";
  } else {
    hinhTamGiac.innerHTML = "Một loại hình tam giác khác :)";
  }
};

